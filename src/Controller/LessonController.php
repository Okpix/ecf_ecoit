<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Lesson;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LessonController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/formations", name="app_lessons")
     */
    public function index(Request $request): Response
    {
        $lessons = $this->entityManager->getRepository(Lesson::class)->findAll();

        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $lessons = $this->entityManager->getRepository(Lesson::class)->findWithSearch($search);
        }

        return $this->render('lesson/index.html.twig', [
            "lessons" => $lessons,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/formations/{slug}", name="app_lesson")
     */
    public function show($slug)
    {
        $lesson = $this->entityManager->getRepository(Lesson::class)->findOneBySlug($slug);

        if (!$lesson) {
            return $this->redirectToRoute('app_lessons');
        }

        return $this->render('lesson/show.html.twig', [
            "lesson" => $lesson,
        ]);
    }
}
