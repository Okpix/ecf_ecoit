<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Lesson;
use App\Entity\Section;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->container->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setController(LessonCrudController::class)->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('ECOIT admin center');
    }


    public function configureMenuItems(): iterable
    {
        return [
            // ...
            MenuItem::linkToRoute('Homepage ECOIT', 'fa fa-home', 'app_home'),
            MenuItem::linkToCrud('Utilisateurs', 'fa fa-user', User::class)
                ->setPermission('ROLE_ADMIN'),
            MenuItem::linkToCrud('Catégories', 'fa fa-list', Category::class)
                ->setPermission(''),
            MenuItem::linkToCrud('Leçons', 'fa fa-book', Lesson::class)
                ->setPermission(''),
        ];
    }
}
