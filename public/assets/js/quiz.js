var quiz = {
    // (A) PROPERTIES
    // (A1) QUESTIONS & ANSWERS
    // Q = QUESTION, O = OPTIONS, A = CORRECT ANSWER
    data: [
        {
            q : "Quel est le titre de cette formation ?",
            o : [
                "formation",
                "test vidéo",
                "la première formation",
                "je ne sais pas"
            ],
            a : 1 // arrays start with 0, so answer is test vidéo
        },
        {
            q : "La vidéo est-elle en lien avec la formation ?",
            o : [
                "oui",
                "non",
            ],
            a : 1
        },
        {
            q : "Quel personnage de dessin animé apparait sur la vidéo ?",
            o : [
                "Le roi lion",
                "Blanche Neige",
                "Luca",
                "Oui-Oui"
            ],
            a : 2
        },
        {
            q : "En quelle 'langue' est écrit le contenu de la formation ?",
            o : [
                "Lorem ipsum",
                "Français",
                "Anglais",
                "Allemand"
            ],
            a : 0
        },
        {
            q : "Cette formation fait partie de quel catégorie ?",
            o : [
                "PHP",
                "React",
                "RESPONSIVE",
                "HTML/CSS"
            ],
            a : 3
        }
    ],

    // (A2) HTML ELEMENTS
    hWrap: null, // HTML quiz container
    hQn: null, // HTML question wrapper
    hAns: null, // HTML answers wrapper

    // (A3) GAME FLAGS
    now: 0, // current question
    score: 0, // current score

    // (B) INIT QUIZ HTML
    init: () => {
        // (B1) WRAPPER
        quiz.hWrap = document.getElementById("quizWrap");

        // (B2) QUESTIONS SECTION
        quiz.hQn = document.createElement("div");
        quiz.hQn.id = "quizQn";
        quiz.hWrap.appendChild(quiz.hQn);

        // (B3) ANSWERS SECTION
        quiz.hAns = document.createElement("div");
        quiz.hAns.id = "quizAns";
        quiz.hWrap.appendChild(quiz.hAns);

        // (B4) GO!
        quiz.draw();
    },

    // (C) DRAW QUESTION
    draw: () => {
        // (C1) QUESTION
        quiz.hQn.innerHTML = quiz.data[quiz.now].q;

        // (C2) OPTIONS
        quiz.hAns.innerHTML = "";
        for (let i in quiz.data[quiz.now].o) {
            let radio = document.createElement("input");
            radio.type = "radio";
            radio.name = "quiz";
            radio.id = "quizo" + i;
            quiz.hAns.appendChild(radio);
            let label = document.createElement("label");
            label.innerHTML = quiz.data[quiz.now].o[i];
            label.setAttribute("for", "quizo" + i);
            label.dataset.idx = i;
            label.addEventListener("click", () => { quiz.select(label); });
            quiz.hAns.appendChild(label);
        }
    },

    // (D) OPTION SELECTED
    select: (option) => {
        // (D1) DETACH ALL ONCLICK
        let all = quiz.hAns.getElementsByTagName("label");
        for (let label of all) {
            label.removeEventListener("click", quiz.select);
        }

        // (D2) CHECK IF CORRECT
        let correct = option.dataset.idx == quiz.data[quiz.now].a;
        if (correct) {
            quiz.score++;
            option.classList.add("correct");
        } else {
            option.classList.add("wrong");
        }

        // (D3) NEXT QUESTION OR END GAME
        quiz.now++;
        setTimeout(() => {
            if (quiz.now < quiz.data.length) { quiz.draw(); }
            else {
                quiz.hQn.innerHTML = `Vous avez répondu correctement à ${quiz.score} des ${quiz.data.length} questions.`;
                quiz.hAns.innerHTML = "Les bonnes réponses sont : " +
                    "<br> - question 1, test vidéo " +
                    "<br> - question 2, non " +
                    "<br> - question 3, Luca " +
                    "<br> - question 4, Lorem ipsum " +
                    "<br> - question 5, HTML/CSS ";
            }
        }, 1000);
    },

    // (E) RESTART QUIZ
    reset : () => {
        quiz.now = 0;
        quiz.score = 0;
        quiz.draw();
    }
};
window.addEventListener("load", quiz.init);