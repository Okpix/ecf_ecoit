Pour déployer le site en local afin de le tester merci de suivre les étapes suivantes dans l’ordre :

1 - Tout d’abord il faudra créer un projet vierge grâce à l’IDE de votre choix.
2 - Il faut ensuite ouvrir un terminal de commande pour se positionner dans le répertoire de votre nouveau projet (exemple : cd /c/xampp/htdocs/NOM_DE_VOTRE_PROJET).
3 - il faut faire la commande suivante pour initier un dépôt git : git init

4 - il faut ensuite lier notre projet au dépôt gitlab via cette commande : git remote add origin https://gitlab.com/Okpix/ecf_ecoit.git

5 - il faut ensuite créer une nouvelle branche sur le projet via cette commande : git checkout -b NOM_DE_VOTRE_BRANCH

6 - une fois la branch faite il faut pull le contenu (le récupérer) avec la commande suivante : git pull origin main 
Il y a une possibilité de bug lié à certains fichiers auto générés au moment du GIT INIT. Si le pull ne fonctionne pas il faut supprimer les fichiers déjà présents sur votre projet et faire un nouveau pull.

7 - le contenu sera importé sur votre local et il faudra ensuite effectuer les deux commandes suivantes : composer install
	       Npm install

8 - il faut ensuite faire un build de l’application via cette commande : npm run build

9 - il faut désormais lancer xampp ou mamp ou un équivalent pour lancer Mysql afin d’accéder à PHPmyAdmin. Une fois dans PHPmyAdmin en local vous pouvez créer une nouvelle base de données. 

Pour la suite deux possibilités soit télécharger la base de données test que vous pouvez télécharger sur mon Gitlab, soit créer une base de donnée à partir des migrations Doctrine. Voilà donc le détails des étapes à suivre pour chacun des cas.

Cas où on télécharge la base de données test :

10 - il faut ensuite importer la base de données que vous pouvez télécharger sur GITLAB ( https://gitlab.com/Okpix/ecf_ecoit/-/blob/main/xuydghsadminecf__3_.sql  ) pour l’importer sur PHPmyAdmin via l’onglet importer puis ajouter un fichier (vous injectez le fichier sql fournit sur le lien précédant). 

11 - il faut ensuite retourner dans votre IDE pour modifier le fichier .env afin d’ajouter les informations pour pouvoir joindre votre base de données en local. Il faut pour cela commenter toutes les lignes inutiles avec un # et laissé une ligne active, exemple : 
DATABASE_URL="mysql://root:@127.0.0.1:3306/TEST_BDD?serverVersion=10.4.22-MariaDB&charset=utf8mb4"
12 - Vous pouvez désormais tester le site en local en lançant en premier le server symfony (si vous avez la symfony CLI) via cette commande : symfony server:start 
Puis il faut lancer le serveur SQL (via xampp mamp ou autre), parfois il faut lancer le serveur apache également. 

Cas où on fait une nouvelle base de données à partir des migrations Doctrine :

10 - afin de créer votre base de donnée je vous invite à vous rendre dans votre fichier .env afin de vérifier qu’une seule ligne correspondant à Mysql est active pour joindre une base de donnée, voilà un exemple : 
DATABASE_URL="mysql://root:@127.0.0.1:3306/TEST_BDD?serverVersion=10.4.22-MariaDB&charset=utf8mb4"
S’il n’y a pas de ligne active vous pouvez en créer une similaire à l’exemple. Attention à votre identifiant (root) et éventuel mot de passe (dans mon cas pas de mot de passe). Vous pouvez également changer le nom de la base de données (dans mon cas TEST_BDD).

11 - Vous pouvez ensuite vous rendre dans un terminal de commande et saisir les commandes suivantes pour créer votre base de données (la commande fournit est fonctionnel si vous avez installé la symfony CLI). 
symfony console doctrine:database:create

12 - Une fois la base de données en place il faut faire la commande suivante pour créer les différentes tables :
symfony console doctrine:migrations:migrate

La console va vous demander si vous êtes certain de vouloir faire cette action il faut répondre oui. Cela va créer votre base de données par rapport à la dernière migration faite via doctrine lors de mon développement. 

Cela veut dire que la base de données aura la même structure que la mienne mais elle n’aura pas de contenu, vous devrez donc créer des utilisateurs/admin/cours… pour tester l’ensemble du site. 
Enfin pour retrouver tout le contenu de mon projet, je vous invite à créer une première catégorie via l’espace admin et de créer une nouvelle leçon avec comme titre (test vidéo, afin d’avoir le slug test-video). Cela aura pour effet de créer une leçon avec un quizz que j’ai fais en javascript. 

Fin de la mise en route en local :

13 - pour tester le site en local vous pouvez désormais vous rendre sur l’adresse suivante : http://127.0.0.1:8000/ 

14 - afin de pouvoir tester toutes les fonctionnalités du site et vous rendre dans le back office je vous invite à créer un compte administrateur via le formulaire d’inscription en choisissant le rôle admin. Pour le reste des instructions de test vous trouverez le détail dans le manuel d’utilisation.

